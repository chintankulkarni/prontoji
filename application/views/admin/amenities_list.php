<!-- page title row starts here-->
<div class="row page-title-row custom-page-title-row">
	<div class="col-xs-12 breadcrums-wrapper">
	    <ol class="breadcrumb">
	        <!-- <li><a href="#">Inquiry Details</a></li> -->
	        <!-- <li class="active">Inquiry Details</li> -->
	    </ol>
	</div>
</div>
<!-- page title row ends here-->
<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <span style="">
                <h2 class="mt-10">Amenities List <a href="<?php echo base_url('admin/Amenities/add_new_amenity'); ?>"><button class="btn btn-primary">+Add New</button></a></h2>
            </span>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
     <?php 
            if(($this->session->flashdata('message_success'))){
                ?>
                <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                </div>
                <?php                
            }
            if(($this->session->flashdata('message_danger'))){
                ?>
                <div class="alert alert-danger  fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Danger!</strong> <?php echo $this->session->flashdata('message_danger'); ?>
                </div>
                <?php                
            }
        ?>
    <div class="col-xs-12 revenue-table-wrapper search-wrapper">       
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table table-responsive" id="tbl_class">
                <thead class="bg-color">
                    <tr>
                        <th>Name</th>                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($amenities) > 0){ ?>
                        <?php foreach($amenities as $key => $one): ?>
                            <tr>                                    
                                <td><?php echo $one['amenity_name']; ?></td>
                                <td><a onclick="confirm('Are you Sure you want to delete this amenity?')?window.location.assign('<?php echo base_url('admin/Amenities/delete_amenity/'.$one['id']);?>'):''"><i class="fa fa-trash" aria-hidden="true"></i></a> &nbsp;&nbsp;
                                <a onclick="confirm('Are you Sure you want to update this amenity?')?window.location.assign('<?php echo base_url('admin/Amenities/edit_amenity/'.$one['id']);?>'):''"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
   </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#tbl_class').dataTable({
            "iDisplayLength": 10,
            "bPaginate": $('#tbl_class tbody tr').length>10,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": "no-sort"}
          ]
        });
        $(".remove_record_btn").on('click',function(){
            var url = base_url + 'admin/Dashboard/remove_record';
            var inquiry_id = $(this).data('inquiry-id');
            if( confirm('Are you sure , you want to remove ?') ){
                $.ajax({
                    url: base_url + 'admin/Dashboard/remove_record', 
                    type: "post", 
                    data: { inquiry_id:inquiry_id }, 
                    success: function (response) { 
                        window.location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) { 
                        console.log(textStatus, errorThrown); 
                    }
                });
            }
        });
    });
</script>

