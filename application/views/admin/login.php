<style type="text/css">
    .msg-block{
        color:#04F507;
        margin-left: 16px;
    }
</style>
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="logo-wrapper">
                    <a href="#">
                        <img src="#" alt="" style="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <form action="<?php echo base_url(); ?>admin/Auth/post_login" class="form-inline" id="login" method="post">
                    <div class="login_box_wrap login_box_wrap_v1">
                        <div class="msg-block">
                            <?php 
                                if($this->session->userdata('msg-forgot-password')){ 
                                    echo "Password has been sent at your email address";
                                    $this->session->unset_userdata('msg-forgot-password'); 
                                }
                            ?>
                        </div>
                        <div class="form-group col-md-5 col-sm-5 col-xs-12  login_input">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email" value="">
                            <span class="help-block"></span>
                        </div>

                        <div class="form-group col-md-5 col-sm-5 col-xs-12 login_input pw_input">
                            <input type="password" class="form-control" id="password"  name="password" placeholder="Password" value="">
                            <span class="help-block"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-sm-12 login_btn">
                            <button type="submit" class="btn btn-default">Login</button>
                        </div>
                        <?php /*
                        <div class="row" style="margin-left:15px;">
                            <div class="col-xs-12 login_links forgot_link pl-none">
                                <a href="<?php echo base_url(); ?>admin/Auth/forgot_password">Forgot password ?</a>
                            </div>
                        </div>
                        */ ?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#login").validate({
            rules:{
                email:{required:true, email:true},
                password:{required:true},
            },
            messages: {
                "email": {
                    required: "Please enter email id"
                },
                "password": {
                    required: "Please enter password"
                }
            },
            submitHandler: function (form){
                form.submit();
            }
        });
    });
</script>