<!-- page title row starts here-->
<div class="row page-title-row custom-page-title-row">
	<div class="col-xs-12 breadcrums-wrapper">
	    <ol class="breadcrumb">
	        <!-- <li><a href="#">Inquiry Details</a></li> -->
	        <!-- <li class="active">Inquiry Details</li> -->
	    </ol>
	</div>
</div>
<!-- page title row ends here-->
<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <span style="">
                <h2 class="mt-10">Property List <a href="<?php echo base_url('admin/Dashboard/add_new_property'); ?>"><button class="btn btn-primary">+Add New</button></a></h2> 
            </span>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper">
     <?php 
            if(($this->session->flashdata('message_success'))){
                ?>
                <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                </div>
                <?php                
            }
            if(($this->session->flashdata('message_danger'))){
                ?>
                <div class="alert alert-danger  fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Danger!</strong> <?php echo $this->session->flashdata('message_danger'); ?>
                </div>
                <?php                
            }
        ?>
    <div class="col-xs-12 revenue-table-wrapper search-wrapper">       
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table table-responsive" id="tbl_class">
                <thead class="bg-color">
                    <tr>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Amount</th>
                        <th>No of beds</th>
                        <th>Amenities</th>                        
                        <th>No of rooms</th>
                        <th>City</th>                        
                        <th>Rent/Sell</th>
                        <th>Description</th>                        
                        <th>Status</th>                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($aPropertyList) > 0){ ?>
                        <?php foreach($aPropertyList as $key => $aPropertyInfo): ?>
                            <tr>                                    
                                <td><?php echo $aPropertyInfo['property_name']; ?></td>
                                <td><img class="previw-img" data-title="<?php echo $aPropertyInfo['property_name']; ?>" style="cursor: pointer;" src="<?php echo base_url('assets/uploads/'.$aPropertyInfo['image']); ?>" width="100" height="100"></td>
                                <td><?php echo $aPropertyInfo['amount']; ?></td> 
                                <td><?php echo $aPropertyInfo['no_of_beds']; ?></td>          
                                <td>
                                <?php 
                                $amenities = $this->db->select('amenity_name')->from('amenities')->where_in('id',explode(',',$aPropertyInfo['amenities']))->get()->result_array();

                                   $amenity = [];
                                  foreach($amenities as $one_amenity)
                                    {                 
                                        $amenity[] = $one_amenity['amenity_name']; 
                                    }  
                                    echo implode(', ',$amenity);
                                ?></td>
                                <td><?php echo $aPropertyInfo['no_of_room']; ?></td>
                                <td><?php echo $this->db->get_where('cities',['id'=> $aPropertyInfo['city_id']])->result()[0]->city_name; ?></td>
                                <td><?php echo $aPropertyInfo['for_rent'] == 1 ? 'Rent':'Sell'; ?></td>
                                <td><?php echo $aPropertyInfo['description']; ?></td>
                                <td><?php echo $aPropertyInfo['is_active'] == 1 ? 'Active':'Inactive'; ?></td>
                                <td><a onclick="confirm('Are you Sure you want to delete this Property?')?window.location.assign('<?php echo base_url('admin/Dashboard/delete_property/'.$aPropertyInfo['id']);?>'):''"><i class="fa fa-trash" aria-hidden="true"></i></a> &nbsp;&nbsp;
                                <a onclick="confirm('Are you Sure you want to update this Property?')?window.location.assign('<?php echo base_url('admin/Dashboard/edit_property/'.$aPropertyInfo['id']);?>'):''"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
   </div>
</div>


<!-- Modal -->
<div id="previw-img-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title-img"></h4>
      </div>
      <div class="modal-body">
        <img id="property_img" src="" style="width:100%">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tbl_class').dataTable({
            "iDisplayLength": 10,
            "bPaginate": $('#tbl_class tbody tr').length>10,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": "no-sort"}
          ]
        });
        $(".remove_record_btn").on('click',function(){
            var url = base_url + 'admin/Dashboard/remove_record';
            var inquiry_id = $(this).data('inquiry-id');
            if( confirm('Are you sure , you want to remove ?') ){
                $.ajax({
                    url: base_url + 'admin/Dashboard/remove_record', 
                    type: "post", 
                    data: { inquiry_id:inquiry_id }, 
                    success: function (response) { 
                        window.location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) { 
                        console.log(textStatus, errorThrown); 
                    }
                });
            }
        });

        $('.previw-img').click(function(event) {
            $('#modal-title-img').text($(this).attr('data-title'));
            $('#property_img').attr('src',$(this).attr('src'));
            $('#previw-img-modal').modal('show');
        });
    });
</script>

