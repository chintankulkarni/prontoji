<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <h2 class="mt-10"> <?php echo isset($data)?'Update':'Add New'; ?> Property</h2>
        </div>
    </div>
</div>

<!-- Basic details -->
<div class="row">
    <div class="col-xs-12 col-sm-12">        
        <form action="<?php echo base_url('admin/Dashboard/save_new_property');?>" id="add_property" name="add_property" method="post"  enctype="multipart/form-data">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Title
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo isset($data)? ( $data[0]->id)?$data[0]->id:'0':'0'; ?>">                                  
                                    <input type="text" class="form-control custom-form-control" name="property_name" id="property_name" value="<?php echo isset($data)?$data[0]->property_name:''; ?>" placeholder="Title" required>
                                </div>
                            </div>
                        </div>
                    </li>

                     <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Amount
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">    
                                    <input type="number" class="form-control custom-form-control" name="amount" id="amount" value="<?php echo isset($data)?$data[0]->amount:''; ?>" placeholder="Amount" required>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Image
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="file" class="form-control custom-form-control" name="image" id="image" placeholder="Upload files" accept=".jpg,.png,.gif,.jpeg,.PNG,.JPG,.JPEG,.GIF" <?php echo isset($data)? '':'required'; ?> >
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    No of rooms
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="no_of_room" id="no_of_room">
                                        <option value="1" <?php echo isset($data)? ( $data[0]->no_of_room == '1')?'selected="selected"':'':''; ?>>1</option>
                                        <option value="2" <?php echo isset($data)? ( $data[0]->no_of_room == '2')?'selected="selected"':'':''; ?> >2</option>
                                        <option value="3" <?php echo isset($data)? ( $data[0]->no_of_room == '3')?'selected="selected"':'':''; ?> >3</option>
                                        <option value=">3" <?php echo isset($data)? ( $data[0]->no_of_room == '>3')?'selected="selected"':'':''; ?> >>3</option>
                                    </select>
                                    <!-- <p class="error"> Please Select Principal</p> -->
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    No of beds
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="no_of_beds" id="no_of_beds">
                                        <option value="1" <?php echo isset($data)? ( $data[0]->no_of_beds == '1')?'selected="selected"':'':''; ?> >1</option>
                                        <option value="2" <?php echo isset($data)? ( $data[0]->no_of_beds == '2')?'selected="selected"':'':''; ?> >2</option>
                                        <option value="3" <?php echo isset($data)? ( $data[0]->no_of_beds == '3')?'selected="selected"':'':''; ?> >3</option>
                                        <option value=">3" <?php echo isset($data)? ( $data[0]->no_of_beds == '>3')?'selected="selected"':'':''; ?> >>3</option>
                                    </select>
                                    <!-- <p class="error"> Please Select Principal</p> -->
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Property type
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="property_type" id="property_type">
                                        <option value="Private" <?php echo isset($data)? ( $data[0]->property_type == 'Private')?'selected="selected"':'':''; ?>>Private</option>
                                        <option value="Shared" <?php echo isset($data)? ( $data[0]->property_type == 'Shared')?'selected="selected"':'':''; ?> >Shared</option>
                                        <option value="Room" <?php echo isset($data)? ( $data[0]->property_type == 'Room')?'selected="selected"':'':''; ?> >Room</option>
                                        <option value="Full House" <?php echo isset($data)? ( $data[0]->property_type == 'Full House')?'selected="selected"':'':''; ?> >Full House</option>
                                        <option value="Other" <?php echo isset($data)? ( $data[0]->property_type == 'Other')?'selected="selected"':'':''; ?>>Other</option>
                                    </select>
                                    <!-- <p class="error"> Please Select Principal</p> -->
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Property For
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="property_for" id="property_for">
                                        <option value="Girls" <?php echo isset($data)? ( $data[0]->property_for == 'Girls')?'selected="selected"':'':''; ?>>Girls</option>
                                        <option value="Boys" <?php echo isset($data)? ( $data[0]->property_for == 'Boys')?'selected="selected"':'':''; ?> >Boys</option>
                                        <option value="Family" <?php echo isset($data)? ( $data[0]->property_for == 'Family')?'selected="selected"':'':''; ?> >Family</option>
                                        <option value="Other" <?php echo isset($data)? ( $data[0]->property_for == 'Other')?'selected="selected"':'':''; ?> >Other</option>
                                    </select>
                                    <!-- <p class="error"> Please Select Principal</p> -->
                                </div>
                            </div>
                        </div>
                    </li>


                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Description
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <textarea class="form-control custom-form-control" rows="3" name="description" id="description" placeholder="Description" required="true"><?php echo isset($data)?$data[0]->description:''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Rent/Sell
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <div class="radio-inline">
                                        <label class="custom_radio">
                                            <input type="radio" name="for_rent" class="for_rent" value="1" <?php echo isset($data)? ( $data[0]->for_rent == 1)?'checked':'':'checked'; ?> >Rent
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label class="custom_radio">
                                            <input type="radio" name="for_rent" class="for_rent"  value="0" <?php echo isset($data)? ( $data[0]->for_rent != 1)?'checked':'':''; ?> >Sell
                                        </label>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Status
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <div class="radio-inline">
                                        <label class="custom_radio">
                                            <input type="radio" name="is_active" class="is_active" value="1" <?php echo isset($data)? ( $data[0]->is_active == 1)?'checked':'':'checked'; ?> >Active
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label class="custom_radio">
                                            <input type="radio" name="is_active" class="is_active" value="0" <?php echo isset($data)? ( $data[0]->is_active != 1)?'checked':'':''; ?> >Inactive
                                        </label>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </li>


                     <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Amenities
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control chosen-select" name="amenities[]" id="amenities" multiple="true" required>

                                        <?php 
                                        if(count($amenities) >0) {
                                            foreach ($amenities as $amenity) {
                                            ?>
                                             <option value="<?php echo $amenity->id?>" <?php if( isset($data) ){ echo in_array($amenity->id,explode(',',$data[0]->amenities))?'selected="selected"':''; } ?> ><?php echo $amenity->amenity_name; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>                                        
                                    </select>
                                    <i id="amenitieserrorsplace"></i>
                                </div>
                            </div>
                        </div>
                    </li>




                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    City
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="city_id" id="city_id">
                                        <option value="">Select</option>
                                        <?php 
                                        if(count($cities) >0) {                     
                                            foreach ($cities as $city) {
                                            ?>
                                             <option value="<?php echo $city->id?>" <?php echo isset($data)? ( $data[0]->city_id == $city->id)?'selected="selected"':'':''; ?> ><?php echo $city->city_name; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>





                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Area
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="area_id" id="area_id">
                                        <option value="">Select</option>
                                        <?php 
                                            if(count($areas) >0) {                     
                                                foreach ($areas as $area) {
                                                ?>
                                                 <option value="<?php echo $area->id?>" <?php echo isset($data)? ( $data[0]->area_id == $area->id)?'selected="selected"':'':''; ?>  ><?php echo $area->area_name; ?></option>
                                                <?php
                                                }
                                            }   
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>





                </ul>

            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn custom-btn custom_btn  custom-save-btn" href="#">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
</script>
<script type="text/javascript">
    $('#city_id').on('change', function(event) {
        if($(this).val().length > 0){
            $.ajax({url: "<?php echo base_url(); ?>admin/Area/area_backend/"+$(this).val(), success: function(result){
                $("#area_id").html(result);
            }});
        }
    });
    $('form[name="add_property"]').validate({
         ignore: [],
   rules: {
          'amount':{required: true, number: true, min:1},
          'city_id':{required: true},
          'area_id':{required: true}
    },
    errorPlacement: function(error, element){
      if(element.attr('name') == "amenities[]"){
        error.insertAfter("#amenitieserrorsplace");      
      }else{
        error.insertAfter(element);
      }
    },
    // submitHandler: function(form) {  
    //     return true;      
    // } 
});
</script>
