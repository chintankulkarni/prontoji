<link href="<?php echo base_url(); ?>assets/admin/css/reset_password.css" rel="stylesheet">
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="logo-wrapper ">
                    <a href="#">
                        <img src="#" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="reset-form-wrap">
            <div class="reset_box_wrap">
                <h1>
                    Forgot Password
                </h1>
                <form class="form" action="<?php echo base_url(); ?>admin/Auth/post_forgot_password" autocomplete="on" id="resetpassword" method="post">
                    <div class="form-group login_input forgot_input">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                        <span class="help-block"></span>
                    </div>
                    <div class="login_btn">
                        <button type="submit" class="btn btn-default reset_password_btn">Reset Password</button>
                    </div>
                    
                    <div class="reset_links">
                        <a href="<?php echo base_url(); ?>admin/Auth/">Back to Login Page</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#resetpassword").validate({
            rules:{
                email:{required:true, email:true},
            },
            messages: {
                "email": {
                    required: "Please enter email id"
                }
            },
            submitHandler: function (form){
                form.submit();
            }
        });
    });
    /*$( document ).ready(function(){
        $(".reset_password_btn").on('click',function(){
            var email = $("#email").val();
                console.log( email );
        });
    });*/
</script>
