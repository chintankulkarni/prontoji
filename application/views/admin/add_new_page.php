<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <h2 class="mt-10"> <?php echo isset($data)?'Update':'Add New'; ?> Page</h2>
        </div>
    </div>
</div>

<!-- Basic details -->
<div class="row">
    <div class="col-xs-12 col-sm-12">        
        <form action="<?php echo base_url('admin/Pages/save_new_Page');?>" id="add_page" name="add_page" method="post"  enctype="multipart/form-data">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Title
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo isset($data)? ( $data[0]->id)?$data[0]->id:'0':'0'; ?>">                                  
                                    <input type="text" class="form-control custom-form-control" name="title" id="title" value="<?php echo isset($data)?$data[0]->title:''; ?>" placeholder="Title" required>
                                </div>
                            </div>
                        </div>
                    </li>                    
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Description
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <textarea class="form-control custom-form-control" rows="3" name="description" id="description" placeholder="Description" ><?php echo isset($data)?$data[0]->description:''; ?></textarea>
                                    <i id="descriptionplacement"></i>
                                </div>
                            </div>
                        </div>
                    </li>                
                </ul>

            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn custom-btn custom_btn  custom-save-btn">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script> ClassicEditor.create( document.querySelector( '#description' ))
.catch( error => {
console.error( error );
});
</script>

<script type="text/javascript">
    $('form[name="add_page"]').validate({
         ignore: [],
   rules: {
          'title':{required: true},
          'description':{required: true, minlength: 20},
    },
    errorPlacement: function(error, element){
      if(element.attr('name') == "description"){
        error.insertAfter("#descriptionplacement");      
      }else{
        error.insertAfter(element);
      }
    },
    // submitHandler: function(form) {  
    //     return true;      
    // } 
});
</script>