<?php //echo "<pre>";var_dump( $oInquiryDetail );exit(); ?>
<style type="text/css">
    .download_xls{
        cursor: pointer;
        color: #6B6BEA;
    }
</style>
<!-- page title row starts here-->
<div class="row page-title-row custom-page-title-row">
	<div class="col-xs-12 breadcrums-wrapper">
	    <ol class="breadcrumb">
	        <!-- <li><a href="#">Inquiry Details</a></li> -->
	        <!-- <li class="active">Inquiry Details</li> -->
	    </ol>
	</div>
</div>
<!-- page title row ends here-->
<!-- Header End-->
<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <span>
                <h2 class="mt-10">Detail View</h2>    
            </span>
        </div>
    </div>
</div>

<!-- table wrapper starts here -->
<div class="row table-contents-wrapper table-records-wrapper" >
    <div class="col-xs-12 revenue-table-wrapper search-wrapper">
        <div class="table-responsive custom-record-table">
            <table class="table table-hover revenue-table" id="tbl_class" >
                <tbody>
                    <?php if(!empty($oInquiryDetail)){ ?>
                        <tr>
                        	<th>Primary phone no</th>
                        	<td><?php echo $oInquiryDetail->primary_phone; ?></td>
                        </tr>
                        <tr>
                        	<th>preferable_time</th>
                        	<td><?php echo $oInquiryDetail->preferable_time; ?></td>
                        </tr>
                        <tr>
                        	<th>email</th>
                        	<td><?php echo $oInquiryDetail->email; ?></td>
                        </tr>
                        <tr>
                        	<th>Income</th>
                        	<td><?php echo $oInquiryDetail->income; ?></td>
                        </tr>
                        <tr>
                        	<th>work_phone</th>
                        	<td><?php echo $oInquiryDetail->work_phone; ?></td>
                        </tr>
                        <tr>
                        	<th>i_get_paid</th>
                        	<td><?php echo $oInquiryDetail->i_get_paid; ?></td>
                        </tr>
                        <tr>
                        	<th>time_employed</th>
                        	<td><?php echo $oInquiryDetail->time_employed; ?></td>
                        </tr>
                        <tr>
                        	<th>from_armed_forces</th>
                        	<td><?php echo $oInquiryDetail->from_armed_forces == 1 ? 'Yes' : 'No'; ?></td>
                        </tr>
                        <tr>
                        	<th>job_title</th>
                        	<td><?php echo $oInquiryDetail->job_title; ?></td>
                        </tr>
                        <tr>
                        	<th>employer_name</th>
                        	<td><?php echo $oInquiryDetail->employer_name; ?></td>
                        </tr>
                        <tr>
                        	<th>monthly_net_income</th>
                        	<td><?php echo $oInquiryDetail->monthly_net_income; ?></td>
                        </tr>
                        <tr>
                        	<th>next_pay_date</th>
                        	<td><?php echo date('d/m/Y',strtotime($oInquiryDetail->next_pay_date)); ?></td>
                        </tr>
                        <tr>
                        	<th>first_name</th>
                        	<td><?php echo $oInquiryDetail->first_name; ?></td>
                        </tr>
                        <tr>
                        	<th>last_name</th>
                        	<td><?php echo $oInquiryDetail->last_name; ?></td>
                        </tr>







                        <tr>
                        	<th>City</th>
                        	<td><?php echo $oInquiryDetail->city; ?></td>
                        </tr>
                        <tr>
                        	<th>address</th>
                        	<td><?php echo $oInquiryDetail->address; ?></td>
                        </tr>
                        <tr>
                        	<th>state</th>
                        	<td><?php echo $oInquiryDetail->state; ?></td>
                        </tr>
                        <tr>
                        	<th>zipcode</th>
                        	<td><?php echo $oInquiryDetail->zipcode; ?></td>
                        </tr>
                        <tr>
                        	<th>loan_amount</th>
                        	<td><?php echo $oInquiryDetail->loan_amount; ?></td>
                        </tr>
                        <tr>
                        	<th>residence_status</th>
                        	<td><?php echo $oInquiryDetail->residence_status; ?></td>
                        </tr>
                        <tr>
                        	<th>birth_date</th>
                        	<td><?php echo date('d/m/Y',strtotime($oInquiryDetail->birth_date)); ?></td>
                        </tr>
                        <tr>
                        	<th>social_security_number</th>
                        	<td><?php echo $oInquiryDetail->social_security_number; ?></td>
                        </tr>
                        <tr>
                        	<th>routing_number</th>
                        	<td><?php echo $oInquiryDetail->routing_number; ?></td>
                        </tr>
                        <tr>
                        	<th>account_number</th>
                        	<td><?php echo $oInquiryDetail->account_number; ?></td>
                        </tr>
                        <tr>
                        	<th>bank_name</th>
                        	<td><?php echo $oInquiryDetail->bank_name; ?></td>
                        </tr>
                        <tr>
                        	<th>bank_phone</th>
                        	<td><?php echo $oInquiryDetail->bank_phone; ?></td>
                        </tr>
                        <tr>
                        	<th>income_type</th>
                        	<td><?php echo $oInquiryDetail->income_type; ?></td>
                        </tr>
                        <tr>
                        	<th>months_at_bank</th>
                        	<td><?php echo $oInquiryDetail->months_at_bank; ?></td>
                        </tr>

                        <tr>
                        	<th>have_direct_deposit</th>
                        	<td><?php echo $oInquiryDetail->have_direct_deposit == 1 ? 'Yes' : 'No'; ?></td>
                        </tr>
                        <tr>
                        	<th>Drivers license</th>
                        	<td><?php echo $oInquiryDetail->license; ?></td>
                        </tr>
                        <tr>
                        	<th>issuing_state</th>
                        	<td><?php echo $oInquiryDetail->issuing_state; ?></td>
                        </tr>
                        <tr>
                        	<th>credit_score</th>
                        	<td><?php echo $oInquiryDetail->credit_score; ?></td>
                        </tr>
                        <tr>
                        	<th>have_own_car</th>
                        	<td><?php echo $oInquiryDetail->have_own_car == 1 ? 'Yes' : 'No'; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
   </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#tbl_class').dataTable({
            "iDisplayLength": 10,
            "bPaginate": $('#tbl_class tbody tr').length>10,
            "bAutoWidth": false,
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": "no-sort"}
          ]
        });
    });
</script>

