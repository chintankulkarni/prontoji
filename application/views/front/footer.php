
    <footer class="footer-bottom">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-12">
                    <!-- Copyright -->
                    <div class="footer-list">
                        <ul>
                            <li>
                                <a href="">About Us</a>
                            </li>
                            <li>
                                <a href="">Contact Us</a>
                            </li>
                            <li>
                                <a href="">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="">Terms &amp; Conditions</a>
                            </li>
                            <li>
                                <a href="">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-12">
                    <!-- Copyright -->
                    <div class="footer-list">
                        <ul>
                            <li>
                                <a href="">Reach Us</a>
                            </li>
                            <li>
                                <a href="">9577 900 500</a>
                            </li>
                            <li>
                                <a href="">support@tmp.com</a>
                            </li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-12">
                    <!-- Social Icons -->
                    <ul class="social-media-icons text-right">
                        <li><a class="fa fa-facebook" href=""></a></li>
                        <li><a class="fa fa-twitter" href=""></a></li>
                        <li><a class="fa fa-linkedin" href=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Container End -->
        <!-- To Top -->
        <div class="top-to">
            <a id="top" class="" href=""><i class="fa fa-angle-up"></i></a>
        </div>
    </footer>
    <!-- JAVASCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/front/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/tether/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/raty/jquery.raty-fa.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/fancybox/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/smoothscroll/SmoothScroll.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
    <script src="<?php echo base_url(); ?>assets/front/js/scripts.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
        
        $('.property-slider').slick({
              dots: false,
              infinite: false,
              speed: 300,
              slidesToShow: 4,
              slidesToScroll: 4,
              arrows: false,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });
            $(".slider-contrl .prev").click(function(){
                $(".property-slider").slick("slickPrev");
            });
            $(".slider-contrl .next").click(function(){
                $(".property-slider").slick("slickNext");
            });
        })
    </script>
</body>

</html> 