<!DOCTYPE html>
<html lang="en">

<head>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prontoji</title>
    <!-- PLUGINS CSS STYLE -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <!-- FAVICON -->
    <link href="<?php echo base_url(); ?>assets/front/img/favicon.png" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="<?php echo base_url(); ?>assets/front/plugins/jquery/jquery.min.js"></script>
  <script>
        var base_url = '<?php echo base_url(); ?>';
    </script>
</head>

<body class="body-wrapper">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg  navigation">
                        <a class="navbar-brand" href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" alt="">
					</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto main-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo base_url(); ?>">Scheduled Visits</a>
                                </li>
                                <?php /*
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard.html"><i class="fa fa-tag active-color" aria-hidden="true"></i> Deals</a>
                                </li> */ ?>
                            </ul>
                            <?php /*
                            <ul class="navbar-nav  mt-10">
                                <li class="nav-item">
                                    <a class="nav-link login-button" href="<?php echo base_url(); ?>">Refer &amp; Earn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link add-button" href="#">List Your House</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link login-button b-none" href="<?php echo base_url(); ?>">Login <i class="fa fa-user-circle-o active-color" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                            */ ?>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!--===============================
=            Hero Area            =
================================-->
