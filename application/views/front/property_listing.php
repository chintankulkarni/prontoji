<!DOCTYPE html>
<html lang="en">

<head>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prontoji::Home</title>
    <!-- PLUGINS CSS STYLE -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
    <!-- Fancy Box -->
    <link href="<?php echo base_url(); ?>assets/front/plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <!-- FAVICON -->
    <link href="<?php echo base_url(); ?>assets/front/img/favicon.png" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="body-wrapper">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg  navigation">
                        <a class="navbar-brand" href="index.html">
                        <img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" alt="">
                    </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto main-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.html">Scheduled Visits</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="dashboard.html"><i class="fa fa-tag active-color" aria-hidden="true"></i> Deals</a>
                                </li>
                            </ul>
                            <ul class="navbar-nav  mt-10">
                                <li class="nav-item">
                                    <a class="nav-link login-button" href="index.html">Refer &amp; Earn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link add-button" href="#">List Your House</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link login-button b-none" href="index.html">Login <i class="fa fa-user-circle-o active-color" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!--===============================
=            Hero Area            =
================================-->
    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block text-left">
                        <h1>Get brokerage free accommodations at amazing prices!</h1>
                        <p>Find a WudStay PG/Flat near you.</p>
                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <form action="#">
                            <div class="row">
                                <!-- Store Search -->
                                <div class="col-lg-10 col-md-12 p-none drop-downs">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Select your City <span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item" href="category.html">Category</a>
                                                        <a class="dropdown-item" href="single.html">Single Page</a>
                                                        <a class="dropdown-item" href="store-single.html">Store Single</a>
                                                        <a class="dropdown-item" href="dashboard.html">Dashboard</a>
                                                        <a class="dropdown-item" href="user-profile.html">User Profile</a>
                                                        <a class="dropdown-item" href="submit-coupon.html">Submit Coupon</a>
                                                        <a class="dropdown-item" href="blog.html">Blog</a>
                                                        <a class="dropdown-item" href="single-blog.html">Single Post</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Select Locality <span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item" href="category.html">Category</a>
                                                        <a class="dropdown-item" href="single.html">Single Page</a>
                                                        <a class="dropdown-item" href="store-single.html">Store Single</a>
                                                        <a class="dropdown-item" href="dashboard.html">Dashboard</a>
                                                        <a class="dropdown-item" href="user-profile.html">User Profile</a>
                                                        <a class="dropdown-item" href="submit-coupon.html">Submit Coupon</a>
                                                        <a class="dropdown-item" href="blog.html">Blog</a>
                                                        <a class="dropdown-item" href="single-blog.html">Single Post</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Select Category <span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item" href="category.html">Category</a>
                                                        <a class="dropdown-item" href="single.html">Single Page</a>
                                                        <a class="dropdown-item" href="store-single.html">Store Single</a>
                                                        <a class="dropdown-item" href="dashboard.html">Dashboard</a>
                                                        <a class="dropdown-item" href="user-profile.html">User Profile</a>
                                                        <a class="dropdown-item" href="submit-coupon.html">Submit Coupon</a>
                                                        <a class="dropdown-item" href="blog.html">Blog</a>
                                                        <a class="dropdown-item" href="single-blog.html">Single Post</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-12 search-btn-wrapper">
                                    <button class="btn btn-main">SEARCH</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>
    <!--===================================
=            Client Slider            =
====================================-->
    <!--===========================================
=            Popular deals section            =
============================================-->
    <section class="popular-deals section bg-light our-pg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Our PG/Flat</h2>
                        <p>Student or a Working professional looking for a stress-free stay?</p>
                        <p>Here we are! Book a WudStay PG/Hostel and get assured of all the key amenities.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- offer 01 -->
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/1.png">
                        </div>
                        <div class="desc">
                            <h4>Unique Design</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/2.png">
                        </div>
                        <div class="desc">
                            <h4>Wi-Fi</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/3.png">
                        </div>
                        <div class="desc">
                            <h4>Food</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/4.png">
                        </div>
                        <div class="desc">
                            <h4>Housekeeping</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/5.png">
                        </div>
                        <div class="desc">
                            <h4>TV</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/6.png">
                        </div>
                        <div class="desc">
                            <h4>24x7 Support</h4>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==========================================
=            All Category Section            =
===========================================-->
   <section class="popular-deals section bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>HIGH DEMEND PROPERTIES</h2>
                    </div>
                </div>
            </div>
            <div class="row property-slider">
                <!-- offer 01 -->
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->                                
                                <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-1.jpg" alt="Card image cap">
                                <ul class="list-inline share-list">
                                    <li class="list-inline-item selected"><a href=""><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
                                    <li class="pull-right"></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <a href="#" class="btn view-more">View More</a>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->                                
                                <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-2.jpg" alt="Card image cap">
                                <ul class="list-inline share-list">
                                    <li class="list-inline-item selected"><a href=""><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
                                    <li class="pull-right"></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <a href="#" class="btn view-more">View More</a>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->                                
                                <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-3.jpg" alt="Card image cap">
                                <ul class="list-inline share-list">
                                    <li class="list-inline-item selected"><a href=""><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
                                    <li class="pull-right"></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <a href="#" class="btn view-more">View More</a>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->                                
                                <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-4.jpg" alt="Card image cap">
                                <ul class="list-inline share-list">
                                    <li class="list-inline-item selected"><a href=""><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
                                    <li class="pull-right"></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <a href="#" class="btn view-more">View More</a>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->
                                <a href="">
                                    <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-1.jpg" alt="Card image cap">
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <ul class="list-inline">
                                        <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                        <li class="list-inline-item"><i class="fa fa-share-alt" aria-hidden="true"></i></li>
                                        <li class="pull-right"><a href="#" class="btn view-more">View More</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->
                                <a href="">
                                    <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-2.jpg" alt="Card image cap">
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <ul class="list-inline">
                                        <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                        <li class="list-inline-item"><i class="fa fa-share-alt" aria-hidden="true"></i></li>
                                        <li class="pull-right"><a href="#" class="btn view-more">View More</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->
                                <a href="">
                                    <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-3.jpg" alt="Card image cap">
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <ul class="list-inline">
                                        <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                        <li class="list-inline-item"><i class="fa fa-share-alt" aria-hidden="true"></i></li>
                                        <li class="pull-right"><a href="#" class="btn view-more">View More</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <!-- product card -->
                    <div class="product-item bg-light">
                        <div class="card">
                            <div class="thumb-content">
                                <!-- <div class="price">$200</div> -->
                                <a href="">
                                    <img class=" img-fluid" src="<?php echo base_url(); ?>assets/front/images/products/products-4.jpg" alt="Card image cap">
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title"><a href="">11inch Macbook Air</a></h4>
                                <ul class="list-inline product-meta">
                                    <li class="list-inline-item d-block">
                                        <a href=""><i class="fa fa-map-marker" aria-hidden="true"></i>Sector 56,Gurgaon</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-bed" aria-hidden="true"></i>Bed</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-wifi" aria-hidden="true"></i>Wifi</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href=""><i class="fa fa-television" aria-hidden="true"></i>TV</a>
                                    </li>
                                </ul>                                
                                <div class="product-ratings">
                                    <ul class="list-inline">
                                        <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                        <li class="list-inline-item"><i class="fa fa-share-alt" aria-hidden="true"></i></li>
                                        <li class="pull-right"><a href="#" class="btn view-more">View More</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-sm-12 text-center slider-contrl">
                    <a class="prev btn">Prev</a>
                    <a class="next btn">Next</a>
                </div>
            </div>
        </div>
    </section>
    <!--============================
=            Footer            =
=============================-->
    <footer class="footer section section-sm">
        <!-- Container Start -->
        <div class="container">
            <div class="row footer-col-wrapper">
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Gurgaon</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <!-- Link list -->
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Delhi</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <!-- Link list -->
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Kota</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <!-- Link list -->
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Banglore</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <!-- Link list -->
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Indore</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container End -->
    </footer>
    <!-- Footer Bottom -->
    <footer class="footer-bottom">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-12">
                    <!-- Copyright -->
                    <div class="footer-list">
                        <ul>
                            <li>
                                <a href="">About Us</a>
                            </li>
                            <li>
                                <a href="">Contact Us</a>
                            </li>
                            <li>
                                <a href="">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="">Terms &amp; Conditions</a>
                            </li>
                            <li>
                                <a href="">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-12">
                    <!-- Copyright -->
                    <div class="footer-list">
                        <ul>
                            <li>
                                <a href="">Reach Us</a>
                            </li>
                            <li>
                                <a href="">9577 900 500</a>
                            </li>
                            <li>
                                <a href="">support@tmp.com</a>
                            </li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-12">
                    <!-- Social Icons -->
                    <ul class="social-media-icons text-right">
                        <li><a class="fa fa-facebook" href=""></a></li>
                        <li><a class="fa fa-twitter" href=""></a></li>
                        <li><a class="fa fa-linkedin" href=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Container End -->
        <!-- To Top -->
        <div class="top-to">
            <a id="top" class="" href=""><i class="fa fa-angle-up"></i></a>
        </div>
    </footer>
    <!-- JAVASCRIPTS -->
    <script src="<?php echo base_url(); ?>assets/front/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/tether/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/raty/jquery.raty-fa.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/fancybox/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/plugins/smoothscroll/SmoothScroll.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
    <script src="<?php echo base_url(); ?>assets/front/js/scripts.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
        
        $('.property-slider').slick({
              dots: false,
              infinite: false,
              speed: 300,
              slidesToShow: 4,
              slidesToScroll: 4,
              arrows: false,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });
            $(".slider-contrl .prev").click(function(){
                $(".property-slider").slick("slickPrev");
            });
            $(".slider-contrl .next").click(function(){
                $(".property-slider").slick("slickNext");
            });
        })
    </script>
</body>

</html> 