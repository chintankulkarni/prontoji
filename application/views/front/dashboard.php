    <?php //if(count($aPropertyList) < 4){ ?>
        <style type="text/css" media="screen">
            .slick-track{
                width: 100%!important;
            }
        </style>
    <?php //}  ?>
    <style type="text/css" media="screen">
        .product-item .thumb-content {
            min-height: 185px;
        }    
    </style>
    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block text-left">
                        <h1>Get brokerage free accommodations at amazing prices!</h1>
                        <p>Find a WudStay PG/Flat near you.</p>
                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search">
                        
                            <div class="row">
                                <!-- Store Search -->
                                <div class="col-lg-12 col-md-12 p-none drop-downs">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="pro_city" data-val="all">All City<span><i class="fa fa-angle-down"></i></span>
												</a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item pro_sub_city" data-val="all">All city</a>
                                                        <?php 
                                                        if(count($cities) >0) {                     
                                                            foreach ($cities as $city) {
                                                            ?>
                                                            <a class="dropdown-item pro_sub_city" data-val="<?php echo $city->id?>"><?php echo $city->city_name; ?></a>
                                                            <?php
                                                            }
                                                        }
                                                        ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="area_id" data-val="all">All area<span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left" id="area_blok">
                                                        <a class="dropdown-item sub_area_id" data-val="all">All area</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="pro_getas" aria-haspopup="true" data-val="all" aria-expanded="false">
													Sell & Rent <span><i class="fa fa-angle-down"></i></span>
												</a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item pro_sub_getas" data-val="all" >Sell & Rent Both</a>
                                                        <a class="dropdown-item pro_sub_getas" data-val="1" >Rent</a>
                                                        <a class="dropdown-item pro_sub_getas" data-val="0" >Sell</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                          
                                    </div>


                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-val="all" id="pro_room">All Rooms <span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item pro_sub_room" data-val="all">All Rooms</a>
                                                        <a class="dropdown-item pro_sub_room" data-val="1">1 BHK</a>
                                                        <a class="dropdown-item  pro_sub_room" data-val="2">2 BHK</a>
                                                        <a class="dropdown-item pro_sub_room" data-val="3">3 BHK</a>
                                                        <a class="dropdown-item pro_sub_room" data-val="4">More than 3 BHK</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-val="all" id="property_type">All Property Type<span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item sub_property_type" data-val="all">All Property Type</a>
                                                        <a class="dropdown-item sub_property_type" data-val="Private">Private</a>
                                                        <a class="dropdown-item  sub_property_type" data-val="Room">Room</a>
                                                        <a class="dropdown-item sub_property_type" data-val="Full House">Full House</a>
                                                        <a class="dropdown-item sub_property_type" data-val="Other">Other</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <div class="no-caret dropdown dropdown-slide">
                                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-val="all" id="property_for">All<span><i class="fa fa-angle-down"></i></span>
                                                </a>
                                                    <!-- Dropdown list -->
                                                    <div class="dropdown-menu dropdown-menu-left">
                                                        <a class="dropdown-item sub_property_for" data-val="all">All</a>
                                                        <a class="dropdown-item sub_property_for" data-val="Girls">Girls</a>
                                                        <a class="dropdown-item  sub_property_for" data-val="Boys">Boys</a>
                                                        <a class="dropdown-item sub_property_for" data-val="Family">Family</a>
                                                        <a class="dropdown-item sub_property_for" data-val="Other">Other</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>











                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                    <input type="number" class="form-control" name="min" id="min" placeholder="enter min amount">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="block d-flex">
                                                <input type="number" class="form-control" name="max" id="max" placeholder="enter max amount">
                                            </div>
                                        </div>
                                    

                                        <div class="col-lg-4 col-md-12 search-btn-wrapper">
                                            <div class="block d-flex">
                                                <button class="btn btn-main" style="padding:  10px;" onclick="search();">SEARCH</button>
                                            </div>
                                        </div>
                                    </div>












                                </div>
                                
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>
    <!--Client Slider-->

    <!--All Category Section-->
    <section class="popular-deals section bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Top Property Collections For You</h2>
                    </div>
                </div>
            </div>
            <div class="row property-slider col-sm-12 col-lg-12">
                <!-- offer 01 -->
                <?php if(count($aPropertyList) > 0){ foreach($aPropertyList as $property){ ?>                 
                    <div class="col-sm-12 col-lg-3">
                        <!-- product card -->
                        <div class="product-item bg-light">
                            <div class="card">
                                <div class="thumb-content">
                                    <!-- <div class="price">$200</div> -->                                
                                    <img class=" img-fluid" src="<?php echo base_url('assets/uploads/'.$property['image']); ?>" alt="Card image cap">
                                    <ul class="list-inline share-list">
                                        <!-- <li class="list-inline-item selected"><a><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a><i class="fa fa-share-alt" aria-hidden="true"></i></a></li> -->
                                        <li class="pull-right"></li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"><a><?php echo $property['property_name'];?></a></h4>
                                    <ul class="list-inline product-meta">
                                        <li class="list-inline-item d-block">
                                            <a><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $this->db->get_where('cities',['id'=> $property['city_id']])->result()[0]->city_name; ?></a>
                                        </li>
                                        <li class="list-inline-item d-block" style="width: 100%;">
                                            <a><i class="fa fa-bed" aria-hidden="true"></i>Bed ( <?php echo ($property['no_of_beds'] != '>3') ? $property['no_of_beds'] : 'More than 3';?> )</a>
                                        </li>
                                        <li class="list-inline-item" style="width: 100%;">
                                            <a><i class="fa fa-address-book" aria-hidden="true"></i>Rooms (<?php echo ($property['no_of_room'] != '>3') ? $property['no_of_room'] : 'More than 3';?> BHK) </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a><i class="fa fa-object-group" aria-hidden="true"></i><?php 
                                    $amenities = $this->db->select('amenity_name')->from('amenities')->where_in('id',explode(',',$property['amenities']))->get()->result_array();

                                       $amenity = [];
                                      foreach($amenities as $one_amenity)
                                        {                 
                                            $amenity[] = $one_amenity['amenity_name']; 
                                        }  
                                        echo implode(', ',$amenity);
                                    ?> </a>
                                        </li>
                                    </ul>                                
                                    <div class="product-ratings">
                                        <a href="<?php echo base_url('details/'.$property['id'])?>" class="btn view-more">View More</a>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }} ?>            
            </div>
            <!-- <div class="row">
                <div class="col-sm-12 text-center slider-contrl">
                    <a class="prev btn">Prev</a>
                    <a class="next btn">Next</a>
                </div>
            </div> --> 
        </div>
    </section>

    <!--Popular deals section-->
    <section class="popular-deals section bg-light our-pg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>What we offer?</h2>
                        <!-- <p>Student or a Working professional looking for a stress-free stay?</p>
                        <p>Here we are! Book a WudStay PG/Hostel and get assured of all the key amenities.</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- offer 01 -->
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/shared.jpg">
                        </div>
                        <div class="desc">
                            <h4>Shared Rooms</h4>
                            <p>Furnished room with shared<br> accommodation for boys and girls.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/private.jpg">
                        </div>
                        <div class="desc">
                            <h4>Private Rooms</h4>     
                            <p>Furnished room with private<br> accommodation for boys and girls.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/family.jpg">
                        </div>
                        <div class="desc">
                            <h4>Full Houses</h4>    
                            <p>Semi/Furnished houses for<br> family and singles.</p>
                        </div>
                    </div>
                </div>
               <!--  <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/4.png">
                        </div>
                        <div class="desc">
                            <h4>Housekeeping</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/5.png">
                        </div>
                        <div class="desc">
                            <h4>TV</h4>                          
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4">                    
                    <div class="single-product">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo base_url(); ?>assets/front/images/icons/6.png">
                        </div>
                        <div class="desc">
                            <h4>24x7 Support</h4>                          
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
<?php if(count(@$SPropertyList) > 0){ ?>
    <!--All Category Section-->
    <section class="popular-deals section bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>Similar Property Collections For You</h2>
                    </div>
                </div>
            </div>
            <div class="row property-slider col-sm-12 col-lg-12">
                <!-- offer 01 -->
                <?php  foreach($SPropertyList as $property){ ?>
                    <div class="col-sm-12 col-lg-3">
                        <!-- product card -->
                        <div class="product-item bg-light">
                            <div class="card">
                                <div class="thumb-content">
                                    <!-- <div class="price">$200</div> -->                                
                                    <img class=" img-fluid" src="<?php echo base_url('assets/uploads/'.$property['image']); ?>" alt="Card image cap">
                                    <ul class="list-inline share-list">
                                        <!-- <li class="list-inline-item selected"><a><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a><i class="fa fa-share-alt" aria-hidden="true"></i></a></li> -->
                                        <li class="pull-right"></li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title"><a><?php echo $property['property_name'];?></a></h4>
                                    <ul class="list-inline product-meta">
                                        <li class="list-inline-item d-block">
                                            <a><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $this->db->get_where('cities',['id'=> $property['city_id']])->result()[0]->city_name; ?></a>
                                        </li>
                                        <li class="list-inline-item d-block" style="width: 100%;">
                                            <a><i class="fa fa-bed" aria-hidden="true"></i>Bed ( <?php echo ($property['no_of_beds'] != '>3') ? $property['no_of_beds'] : 'More than 3';?> )</a>
                                        </li>
                                        <li class="list-inline-item" style="width: 100%;">
                                            <a><i class="fa fa-address-book" aria-hidden="true"></i>Rooms (<?php echo ($property['no_of_room'] != '>3') ? $property['no_of_room'] : 'More than 3';?> BHK) </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a><i class="fa fa-object-group" aria-hidden="true"></i><?php 
                                    $amenities = $this->db->select('amenity_name')->from('amenities')->where_in('id',explode(',',$property['amenities']))->get()->result_array();

                                       $amenity = [];
                                      foreach($amenities as $one_amenity)
                                        {                 
                                            $amenity[] = $one_amenity['amenity_name']; 
                                        }  
                                        echo implode(', ',$amenity);
                                    ?> </a>
                                        </li>
                                    </ul>                                
                                    <div class="product-ratings">
                                        <a href="<?php echo base_url('details/'.$property['id'])?>" class="btn view-more">View More</a>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>            
            </div>
        </div>
    </section>    
    <?php } ?>

    <script type="text/javascript">
        $(function(){

            $('.pro_sub_city').click(function(event) {
                $('#pro_city').attr('data-val',$(this).attr('data-val'));
                $('#pro_city').text($(this).text());
                var val = $('#pro_city').attr('data-val');
                console.log(val);
                if(val != "all"){
                    $.ajax({url: "<?php echo base_url(); ?>home/area_frontend/"+val, success: function(result){
                        $("#area_blok").html(result);
                         $('.sub_area_id').click(function(event) {
                            $('#area_id').attr('data-val',$(this).attr('data-val'));
                            $('#area_id').text($(this).text());
                        });
                    }});
                }
            });
            $('.pro_sub_room').click(function(event) {
                $('#pro_room').attr('data-val',$(this).attr('data-val'));
                $('#pro_room').text($(this).text());
            });
            $('.pro_sub_getas').click(function(event) {
                $('#pro_getas').attr('data-val',$(this).attr('data-val'));
                $('#pro_getas').text($(this).text());
            });
            $('.sub_property_type').click(function(event) {
                $('#property_type').attr('data-val',$(this).attr('data-val'));
                $('#property_type').text($(this).text());
            });
            $('.sub_property_for').click(function(event) {
                $('#property_for').attr('data-val',$(this).attr('data-val'));
                $('#property_for').text($(this).text());
            });
            //  $('.pro_sub_city').click(function(event) {
                
            // });
            $('.sub_area_id').click(function(event) {
                $('#area_id').attr('data-val',$(this).attr('data-val'));
                $('#area_id').text($(this).text());
            });
        });
        function search(){
            var link = base_url +'?city='+$('#pro_city').attr('data-val')+'&area='+$('#area_id').attr('data-val')+'&roomon='+$('#pro_getas').attr('data-val')+'&roomas='+$('#pro_room').attr('data-val')+'&property_type='+$('#property_type').attr('data-val')+'&property_for='+$('#property_for').attr('data-val')+'&min='+$('#min').val()+'&max='+$('#max').val();
            window.location = link;
            console.log(link);
        }
    </script>

    <!--Footer starts-->
    <!-- <footer class="footer section section-sm">
        <div class="container">
            <div class="row footer-col-wrapper">
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Gurgaon</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Delhi</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Kota</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Banglore</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="block">
                        <h4>PGs in Indore</h4>
                        <ul>
                            <li><a href="#">PG near Golf Course Road</a></li>
                            <li><a href="#">PG on MG Rorad</a></li>
                            <li><a href="#">PG in Sector 14,15,23</a></li>                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer> -->
    <!-- Footer ends -->