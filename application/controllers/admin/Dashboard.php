<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	private $sTable1 = "properties";
	private $sTable2 = "users";

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){
        $sql = "select * from ".$this->sTable1." where is_active = 1";
        $aData['aPropertyList'] = $this->db->query( $sql )->result_array();
        $this->load->template_admin_general('admin/property_list',$aData);
	}
    public function add_new_property(){
        $data['amenities'] = $this->db->get_where('amenities',['is_active'=> 1])->result();
        $data['cities'] = $this->db->get_where('cities',['is_active'=> 1])->result();
        $this->load->template_admin_general('admin/add_new_property',$data);
    }
    public function edit_property(){    
        $data['data'] = $this->db->get_where('properties',['id'=> $this->uri->segment(4)])->result();
        $data['amenities'] = $this->db->get_where('amenities',['is_active'=> 1])->result();
        $data['cities'] = $this->db->get_where('cities',['is_active'=> 1])->result();
        $data['areas'] = $this->db->get_where('areas',['is_active'=>1,'city_id'=> $data['data'][0]->city_id])->result(); 
        $this->load->template_admin_general('admin/add_new_property',$data);
    }
    public function delete_property(){   
        $this->db->where('id', $this->uri->segment(4));        
        if($this->db->delete('properties')){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/Dashboard');   
    }
    public function save_new_property(){
        $propdata = $this->input->post();
        $propdata['amenities'] = implode(",",$propdata['amenities']);       
        $config['upload_path']   = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        
        $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('image'))
            {
                $error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                // var_dump($data["upload_data"]['orig_name']);
                // exit();
                $propdata['image'] = $data["upload_data"]['orig_name'];
                //$this->load->view('upload_success', $data);
            }
            $result = 0;
            $optration = "";
            if($propdata['id'] > 0){
                $result = $this->db->update('properties', $propdata,['id'=>$propdata['id']]);
                $optration = "Updatede";
            }else{
                $result = $this->db->insert('properties', $propdata);
                $optration = "inserted";
            }
            
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/Dashboard');        
    }
	/*public function download_xls(){
        $sql = "select * from ".$this->sTable1." where deleted_at IS NULL order by id desc";
        $aInquiryList = $this->db->query( $sql )->result_array();
		
        $aFinalaInquiryList = array();
        $aFinalaInquiryList[0]['primary_phone'] = "primary_phone";
        $aFinalaInquiryList[0]['preferable_time'] = "preferable_time";
        $aFinalaInquiryList[0]['email'] = "email";
        $aFinalaInquiryList[0]['income'] = "income";
        $aFinalaInquiryList[0]['work_phone'] = "work_phone";
        $aFinalaInquiryList[0]['i_get_paid'] = "i_get_paid";
        $aFinalaInquiryList[0]['time_employed'] = "time_employed";
        $aFinalaInquiryList[0]['from_armed_forces'] = "from_armed_forces";
        $aFinalaInquiryList[0]['job_title'] = "job_title";
        $aFinalaInquiryList[0]['employer_name'] = "employer_name";
        $aFinalaInquiryList[0]['monthly_net_income'] = "monthly_net_income";
        $aFinalaInquiryList[0]['next_pay_date'] = "next_pay_date";
        $aFinalaInquiryList[0]['first_name'] = "first_name";
        $aFinalaInquiryList[0]['last_name'] = "last_name";
        $aFinalaInquiryList[0]['city'] = "city";
        $aFinalaInquiryList[0]['address'] = "address";
        $aFinalaInquiryList[0]['state'] = "state";
        $aFinalaInquiryList[0]['zipcode'] = "zipcode";
        $aFinalaInquiryList[0]['loan_amount'] = "loan_amount"; 
        $aFinalaInquiryList[0]['residence_status'] = "residence_status";
        $aFinalaInquiryList[0]['birth_date'] = "birth_date";
        $aFinalaInquiryList[0]['social_security_number'] = "social_security_number";
        $aFinalaInquiryList[0]['routing_number'] = "routing_number";
        $aFinalaInquiryList[0]['account_number'] = "account_number";
        $aFinalaInquiryList[0]['bank_name'] = "bank_name";
        $aFinalaInquiryList[0]['bank_phone'] = "bank_phone";
        $aFinalaInquiryList[0]['income_type'] = "income_type"; 
        $aFinalaInquiryList[0]['months_at_bank'] = "months_at_bank";
        $aFinalaInquiryList[0]['have_direct_deposit'] = "have_direct_deposit";
        $aFinalaInquiryList[0]['license'] = "Drivers license";
        $aFinalaInquiryList[0]['issuing_state'] = "issuing_state";
        $aFinalaInquiryList[0]['credit_score'] = "credit_score";
        $aFinalaInquiryList[0]['have_own_car'] = "have_own_car";
            
        $i = 1;    
        foreach($aInquiryList as $key => $aInquiry):
            $aFinalaInquiryList[$i] = $aInquiry;
            $aFinalaInquiryList[$i]['from_armed_forces'] = $aInquiry['from_armed_forces'] == 1 ? 'Yes' : 'No'; 
            $aFinalaInquiryList[$i]['have_direct_deposit'] = $aInquiry['have_direct_deposit'] == 1 ? 'Yes' : 'No'; 
            $aFinalaInquiryList[$i]['have_own_car'] = $aInquiry['have_own_car'] == 1 ? 'Yes' : 'No'; 
            unset( $aFinalaInquiryList[$i]['id'] );   
            $i++;
        endforeach;
         
        $this->excel->setActiveSheetIndex(0); 
        $this->excel->getActiveSheet()->getStyle('A1:AG1')->getFont()->setBold(true);
        $filename = 'inquiry_details_list.xls'; //save our workbook as this file name
        $this->excel->getActiveSheet()->fromArray($aFinalaInquiryList);

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');   
    }    
	public function view_inquiry_detail(){
        $aData['aInquiryDetails'] = array_reverse( $this->db_common->get_result( $this->sTable1 ) );
    	$aWhere['id'] = $this->uri->segment(4);
    	$aData['oInquiryDetail'] = $this->db_common->get_row( $this->sTable1 , $aWhere );
	    $this->load->template_admin_template('admin/detail_inquiry_view',$aData);   		
	}*/
	public function remove_record(){
        if( $this->input->post('inquiry_id') ){
            $aData['deleted_at'] = date('Y-m-d h:i:s');
            $aWhere['id'] = $this->input->post('inquiry_id');
            $this->db_common->update( $this->sTable1 , $aData , $aWhere );
        }        
    }
}	
