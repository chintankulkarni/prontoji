<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amenities extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){        
        $aData['amenities'] = $this->db->get_where('amenities',['is_active'=> 1])->result_array();
        $this->load->template_admin_general('admin/amenities_list',$aData);
	}
    public function add_new_amenity(){       
        $this->load->template_admin_general('admin/add_new_amenity');
    }
    public function edit_amenity(){    
        $data['data'] = $this->db->get_where('amenities',['id'=> $this->uri->segment(4)])->result();
        $this->load->template_admin_general('admin/add_new_amenity',$data);
    }
    public function delete_amenity(){  
        if($this->db->update('amenities', ['is_active'=>0],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/Amenities');   
    }
    public function save_new_amenity(){
        $propdata = $this->input->post();
            $result = 0;
            $optration = "";
            if($propdata['id'] > 0){
                $result = $this->db->update('amenities', $propdata,['id'=>$propdata['id']]);
                $optration = "Updatede";
            }else{
                $result = $this->db->insert('amenities', $propdata);
                $optration = "inserted";
            }
            
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/Amenities');        
    }
}	
