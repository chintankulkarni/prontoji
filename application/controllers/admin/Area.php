<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){        
        $aData['areas'] = $this->db->get_where('areas',['is_active'=> 1])->result_array();
        $this->load->template_admin_general('admin/areas_list',$aData);
	}
    public function add_new_area(){       
        $aData['cities']  = $this->db->get_where('cities',['is_active'=> 1])->result();
        $this->load->template_admin_general('admin/add_new_area',$aData);
    }
    public function edit_area(){    
        $data['cities'] = $this->db->get_where('cities',['is_active'=> 1])->result();
        $data['data'] = $this->db->get_where('areas',['id'=> $this->uri->segment(4)])->result();
        $this->load->template_admin_general('admin/add_new_area',$data);
    }
    public function delete_area(){  
        if($this->db->update('areas', ['is_active'=>0],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/area');   
    }
    public function save_new_area(){
        $propdata = $this->input->post();
        // var_dump($propdata);
        // exit;
            $result = 0;
            $optration = "";
            $propdata['is_active'] = 1;
            if($propdata['id'] > 0){
                $result = $this->db->update('areas', $propdata,['id'=>$propdata['id']]);
                $optration = "Updated";
            }else{
                $result = $this->db->insert('areas', $propdata);
                $optration = "inserted";
            }
            
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/area');        
    }
    public function area_backend(){
        $areas = $this->db->get_where('areas',['is_active'=>1,'city_id'=> $this->uri->segment(4)])->result();        
        if(count($areas) >0) {                     
            foreach ($areas as $area) {
            ?>
             <option value="<?php echo $area->id?>" ><?php echo $area->area_name; ?></option>
            <?php
            }
        }
    }
}	
