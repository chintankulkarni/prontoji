<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cities extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){        
        $aData['cities'] = $this->db->get_where('cities',['is_active'=> 1])->result_array();
        $this->load->template_admin_general('admin/cities_list',$aData);
	}
    public function add_new_city(){       
        $this->load->template_admin_general('admin/add_new_city');
    }
    public function edit_city(){    
        $data['data'] = $this->db->get_where('cities',['id'=> $this->uri->segment(4)])->result();
        $this->load->template_admin_general('admin/add_new_city',$data);
    }
    public function delete_city(){  
        if($this->db->update('cities', ['is_active'=>0],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/cities');   
    }
    public function save_new_city(){
        $propdata = $this->input->post();
            $result = 0;
            $optration = "";
            if($propdata['id'] > 0){
                $result = $this->db->update('cities', $propdata,['id'=>$propdata['id']]);
                $optration = "Updatede";
            }else{
                $result = $this->db->insert('cities', $propdata);
                $optration = "inserted";
            }
            
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/cities');        
    }
}	
