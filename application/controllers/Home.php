<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct ();
		$this->load->model('common_model', 'db_common' );
		$this->load->library('email'); 
	}
	public function index(){		
		$search['is_active'] = 1;
		if($_GET){
			if(isset($_GET['city']) &&  is_numeric($_GET['city'])){
				$search['city_id'] = $_GET['city'];
				$aData['SPropertyList'] = $this->db->get_where('properties', array('city_id'=>$_GET['city']), 4)->result_array();
			}
			if(isset($_GET['area']) &&  is_numeric($_GET['area'])){
				$search['area_id'] = $_GET['area'];
			}
			if(isset($_GET['roomon']) && is_numeric($_GET['roomon'])){
				$search['for_rent'] = $_GET['roomon'];
			}
			if(isset($_GET['roomas']) &&  is_numeric($_GET['roomas'])){
				$search['no_of_room'] = $_GET['roomas'] > 3 ? '>3' : $_GET['roomas'];
			}
			if( isset($_GET['property_type']) &&  ($_GET['property_type'] != "all") ){
				$search['property_type'] = $_GET['property_type'];
			}
			if( isset($_GET['property_for']) &&  ($_GET['property_for'] != "all") ){
				$search['property_for'] = $_GET['property_for'];
			}			
		}
		$aData['cities'] = $this->db->get_where('cities',['is_active'=> 1])->result();
        $query = $this->db->select('*')->from('properties')->where($search);
        if($_GET){
			if(isset($_GET['min']) &&  is_numeric($_GET['min'])){
				$query->where('amount >=',$_GET['min']);
			}
			if(isset($_GET['max']) &&  is_numeric($_GET['max'])){
				$query->where('amount <=',$_GET['max']);
			}
		}	
        $aData['aPropertyList'] = $query->get()->result_array();
		$this->load->template_front('front/dashboard',$aData);		
	}
	public function details(){
		$data['id'] = $this->uri->segment(2);
		$data['data'] = $this->db->get_where('properties',['id'=> $this->uri->segment(2)])->result();
		$this->load->template_front('front/details',$data);		
	}

	public function Property_interest(){
		$data = $this->input->post();
		$data['admin'] = $this->config->item('admin_mail');
		$property = $this->db->get_where('properties',['id'=> $data['id']])->result_array()[0];
		$message = "Hello Admin,<br><br>".$data['name']." intersted in ".$property['property_name']." located at ".$this->db->get_where('cities',['id'=> $property['city_id']])->result()[0]->city_name."<br><br><b>Visitor's Information.<b><br>Visitor Name : ".$data['name']."<br>Phone : ".$data['phone']."<br>Email : ".$data['email']."<br><br><b>Schedule</b><br><br>Date : ".$data['date']."<br><br>Thank you";
		$subject = 'Enquiry for property | '. $property["property_name"];
		$to = $data['admin'];
		$from_email = $data['email'];
		$from_name = $data['name'];		
		$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.mailtrap.io',
		  'smtp_port' => 465,
		  'smtp_user' => '92740fc85ede1a',
  		  'smtp_pass' => '42498439ec7302',
		  'crlf' => "\r\n",
		  'newline' => "\r\n"	
		);
		$this->email->initialize($config);

		$this->email->from($from_email, $from_name);
		$this->email->to($to);
		$this->email->subject( $subject );
		$this->email->message($message);   	
		$this->email->set_mailtype("html");	
		//$this->email->send();

	   // $header = "From:".$from_email." \r\n";
	     
	    //$header .= "MIME-Version: 1.0\r\n";
	    //$header .= "Content-type: text/html\r\n";
	     // var_dump(mail ($to,$subject,$message,$header));
	     // exit;
	    //$retval = mail ($to,$subject,$message,$header);
        if($this->email->send()){
            $this->session->set_flashdata('message_success', 'Mail Successfully Send.');
        }else{
            $this->session->set_flashdata('message_danger', 'Mail Not Send.');
        }
        redirect('details/'.$data["id"]);
	}
	public function property_detail(){
		$this->load->view('front/property_detail');
	}
	public function area_frontend(){
        $areas = $this->db->get_where('areas',['is_active'=>1,'city_id'=> $this->uri->segment(3)])->result();        
        if(count($areas) >0) {     
        echo '<a class="dropdown-item sub_area_id" data-val="all">All area</a>';
            foreach ($areas as $area) {
            ?>
            <a class="dropdown-item sub_area_id" data-val="<?php echo $area->id?>" ><?php echo $area->area_name; ?></a>
            <?php
            }
        }
    }
}