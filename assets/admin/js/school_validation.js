
// Validations for add new school , edit school

$("#add_school").validate({
    errorClass: 'help-block',
    errorElement: 'span',

    rules:{
        school_name: { required:true },
        city: { required:true },
        contact_no: { required:true },
        email: { required:true,email:true},
        principal_id: { required:true },
    },
    messages:{
        "school_name" : { required : "Please Enter School Name"},
        "city" : { required : "Please Enter City" },
        "contact_no" : { required : "Please Enter Mobile No" },
        "email" : { required : "Please Enter Email-id" },
        "principal_id": { required : "Please Choose Principal" },
    },
    highlight: function( element , errorClass , validClass ){
        $(element).parents('div.form-group').addClass(errorClass);
    },
});
    