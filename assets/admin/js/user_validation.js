
// Validation for Add New User - Edit User form
    
$("#add_user_form").validate({
    errorClass:'help-block',
    errorElement:'span',

    rules:{
        first_name:{ required:true },
        last_name:{ required:true },
        role_id:{ required:true },
        email:{ required:true,email: true },
        password: { required:true },
        contact_no:{ required:true },
    },
    messages: {
        "first_name": { required: "Please Enter First Name" },
        "last_name": { required: "Please Enter Last Name" },
        "role_id": { required: "Please Choose Role" },
        "email": { required: "Please Enter Email address" },
        "password": { required: "Please Enter Password" },
        "contact_no": { required: "Please Enter Mobile Number" },
    },
    highlight: function (element, errorClass, validClass) 
    { 
        $(element).parents("div.form-group")
                  .addClass(errorClass); 
    }, 
});