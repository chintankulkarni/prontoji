-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 23, 2018 at 04:29 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prontoji`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
CREATE TABLE IF NOT EXISTS `amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `amenity_name`, `is_active`, `created_at`) VALUES
(1, 'TV', 1, '2018-03-15 16:09:45'),
(2, 'WiFi', 1, '2018-03-15 16:09:45'),
(3, 'Frige', 1, '2018-03-21 16:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `city_id` int(16) NOT NULL,
  `area_name` varchar(255) NOT NULL,
  `is_active` int(16) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `city_id`, `area_name`, `is_active`, `created_at`) VALUES
(1, 1, 'shahpur', 1, '2018-04-23 16:02:01');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) NOT NULL,
  `is_active` int(16) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `is_active`, `created_at`) VALUES
(1, 'Ahmedabad', 1, '2018-03-15 16:09:20'),
(2, 'Surat', 1, '2018-03-15 16:09:20'),
(3, 'Vadodara', 1, '2018-04-03 16:22:44'),
(4, 'Bharuch', 1, '2018-04-03 16:23:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `description`, `created_at`) VALUES
(12, 'fghfgh', '<p>gfghfghffghfghfghfgh</p>', '2018-04-03 16:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) NOT NULL,
  `no_of_beds` enum('1','2','3','>3') NOT NULL DEFAULT '1',
  `no_of_room` enum('1','2','3','>3') NOT NULL DEFAULT '1',
  `city_id` int(11) DEFAULT NULL,
  `amenities` text NOT NULL,
  `amount` int(16) NOT NULL,
  `for_rent` tinyint(1) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `property_type` enum('Private','Shared','Room','Full House','Other') NOT NULL,
  `property_for` enum('Girls','Boys','Family','Other') NOT NULL,
  `area_id` int(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `property_name`, `no_of_beds`, `no_of_room`, `city_id`, `amenities`, `amount`, `for_rent`, `description`, `image`, `is_active`, `created_at`, `property_type`, `property_for`, `area_id`) VALUES
(1, 'Title 1', '1', '1', 1, '1', 1000, 1, 'Descriptionv 1', '4_1520251419.png', 1, '2018-03-17 03:32:40', 'Private', 'Girls', 0),
(2, 'Title 2', '2', '1', 2, '2', 2000, 1, 'Description 2', '4_1520251205.jpg', 1, '2018-03-17 03:33:14', 'Private', 'Girls', 0),
(3, 'Title 3', '3', '1', 1, '1,2', 3000, 0, 'Description 3', '4_1520251117.jpg', 1, '2018-03-17 03:33:56', 'Other', 'Boys', 0),
(4, 'Title 4', '>3', '1', 2, '1,2', 4000, 0, 'Description 4', '4_1520251205.jpg', 1, '2018-03-17 03:34:44', 'Private', 'Girls', 0),
(5, 'Title 5', '1', '1', 1, '1', 5000, 1, 'Description 5', 'bg1.jpg', 1, '2018-03-17 03:39:39', 'Private', 'Girls', 0),
(6, 'Title 6', '1', '2', 2, '2', 6000, 0, 'Description 6', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:40:07', 'Private', 'Girls', 0),
(7, 'Title 7', '1', '3', 2, '2', 7000, 0, 'Description 7', '1452792-1920x1080.jpg', 1, '2018-03-17 03:40:43', 'Private', 'Girls', 0),
(8, 'Title 8', '1', '3', 2, '1,2', 8000, 1, 'Description 8', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:41:26', 'Private', 'Girls', 0),
(9, 'Title 9', '1', '3', 2, '1,2', 9000, 1, 'Description 9', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:42:06', 'Private', 'Girls', 0),
(10, 'Title 10', '1', '>3', 2, '1,2', 10000, 1, 'Description 10', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:42:36', 'Private', 'Girls', 0),
(11, 'Title 11', '2', '2', 2, '1,2', 11000, 1, 'Description 11', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:43:14', 'Private', 'Girls', 0),
(12, 'Title 12', '2', '3', 2, '1,2', 12000, 1, 'Description 12', 'colors_sky_space_clouds_stars_2560x1600.jpg', 1, '2018-03-17 03:43:55', 'Private', 'Girls', 0),
(13, 'rtyrt', '1', '1', 1, '3', 5645, 1, '456', '1Untitled.png', 1, '2018-03-24 07:28:38', 'Private', 'Girls', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'admin@example.com', 'password');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
